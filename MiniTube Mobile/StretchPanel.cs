﻿// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Public License (Ms-PL).
// Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
// All other rights reserved.

// Modified slightly for Windows Runtime support.

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace System.Windows.Controls
{
    /// <summary>
    /// Arranges child elements around the edges of the panel.  Optionally, 
    /// last added child element can occupy the remaining space.
    /// </summary>
    /// <QualityBand>Stable</QualityBand>
    public class StretchPanel : Panel
    {
       
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Windows.Controls.DockPanel" /> class.
        /// </summary>
        public StretchPanel()
        {
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Uri), typeof(StretchPanel), new PropertyMetadata(Orientation.Vertical));
        public Orientation Orientation
        {
            get
            {
                return (Orientation)this.GetValue(OrientationProperty);
            }
            set
            {
                this.SetValue(OrientationProperty, value);
            }
        }

        

        /// <summary>
        /// Arranges the child elements of the
        /// <see cref="T:System.Windows.Controls.DockPanel" /> control.
        /// </summary>
        /// <param name="arrangeSize">
        /// The area in the parent element that the
        /// <see cref="T:System.Windows.Controls.DockPanel" /> should use to
        /// arrange its child elements.
        /// </param>
        /// <returns>
        /// The actual size of the
        /// <see cref="T:System.Windows.Controls.DockPanel" /> after the child
        /// elements are arranged. The actual size should always equal
        /// <paramref name="arrangeSize" />.
        /// </returns>
        [SuppressMessage("Microsoft.Naming", "CA1725:ParameterNamesShouldMatchBaseDeclaration", MessageId = "0#", Justification = "Compat with WPF.")]
        protected override Size ArrangeOverride(Size arrangeSize)
        {
            double left = 0.0;
            double top = 0.0;
            double right = 0.0;
            double bottom = 0.0;

            // Arrange each of the Children
            UIElementCollection children = Children;
            int index = 0;
            foreach (UIElement element in children)
            {
                // Determine the remaining space left to arrange the element
                Rect remainingRect = new Rect(
                    left,
                    top,
                    Math.Max(0.0, arrangeSize.Width - left - right),
                    Math.Max(0.0, arrangeSize.Height - top - bottom));

                Size desiredSize = arrangeSize;
                if (Orientation == global::Windows.UI.Xaml.Controls.Orientation.Vertical)
                {
                    top += desiredSize.Height / Children.Count;
                    remainingRect.Height = desiredSize.Height / Children.Count;
                }
                else
                {
                    left += desiredSize.Width / Children.Count;
                    remainingRect.Width = desiredSize.Width / Children.Count;
                }

                element.Arrange(remainingRect);
                index++;
            }

            return arrangeSize;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Security.Authentication.Web;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YouTube.API.Util;
using YouTube.API;
using YouTube.API.Parameters;
using YouTube.API.DataModel.Search;
using YouTube.API.DataModel.Videos;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace MiniTube_Mobile
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            load();
        }

        public void load()
        {
            ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();


            List<StandardFeeds> feeds = new List<StandardFeeds>();

            var paramenter = new VideosParameters()
            {
                chart = "mostPopular",
                part = "snippet,statistics",
                regionCode = System.Globalization.RegionInfo.CurrentRegion.Name,
                maxResults = 50
            };

            
            feeds.Add(new StandardFeeds()
            {
                Name = LanguageLoader.GetString("PopularLabel"),
                Feed = null
            });

            // feedsCollection.Source = feeds;
            l.ItemsSource = new IncrementalLoader<VideosResponse, VideosParameters>(Videos.List, paramenter);

        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }
        public async Task LoadSubscriptionFeed()
        {
            List<StandardFeeds> feeds = new List<StandardFeeds>();
            try
            {
                await Login.GetToken(async (token)=> 
                {
                    //IsLoading = true;
                    YouTube.API.OAuth2.Profile p = new YouTube.API.OAuth2.Profile();
                    var channels = await YouTube.API.Subscriptions.List(token, new YouTube.API.Parameters.SubscriptionsParameters()
                    {
                        channelId = await p.GetUserId(token),
                        maxResults = 20
                    });
                    foreach (var item in channels.items)
                    {
                        try
                        {
                            feeds.Add(new StandardFeeds()
                            {
                                Name = item.snippet.title,
                                /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                                Feed = (await YouTube.API.Search.List(new YouTube.API.Parameters.SearchParameters()
                                {
                                    channelId = item.snippet.resourceId.channelId,
                                    order = YouTube.API.Parameters.SearchParameters.OrderType.date,
                                    maxResults = 6,
                                    type = "video"

                                }
                            )).items.ToObservableCollection()
                            });
                        }
                        catch { }
                    }
                    feedsCollection.Source = feeds;
                    //SetContent(typeof(Home), feeds);

                });                
            }
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
            finally
            {
                //IsLoading = false;
            }

            //return feeds;
        }
        dynamic Sections;

        private void l_ItemClick(object sender, ItemClickEventArgs e)
        {
            Frame.Navigate(typeof(ViewVideo), e.ClickedItem);

            
        }

        private async void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (sender as ListView).SelectedItem as ListViewItem;
            //IsLoading = true;
            try
            {
                if (item != null)
                {
                    //ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                    switch (item.Tag.ToString())
                    {
                        //case "home":
                        //    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Popular", null, 1);
                        //    if (home == null)
                        //        home = await LoadFeed("mostPopular", null, LanguageLoader.GetString("PopularLabel"));
                        //    else
                        //        ContentContainer.Navigate(typeof(Home), home);
                        //    break;
                        case "subscriptions":
                            //GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Subscriptions Feed", null, 1);
                            if (Sections.Subscriptions == null)
                            {
                                await LoadSubscriptionFeed();
                                //try
                                //{
                                //    foreach (var i in (List<StandardFeeds>)subscriptions)
                                //        ((List<StandardFeeds>)home).Add(i);
                                //}
                                //catch { }
                            }
                            //else ContentContainer.Navigate(typeof(Home), subscriptions);
                            break;
                        //case "music":
                        //    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Music", null, 1);
                        //    if (music == null)
                        //        music = await LoadFeed("mostPopular", "10", LanguageLoader.GetString("MusicLabel"));
                        //    else ContentContainer.Navigate(typeof(Home), music);
                        //    break;
                        //case "sports":
                        //    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Sports", null, 1);
                        //    if (sports == null)
                        //        sports = await LoadFeed("mostPopular", "17", LanguageLoader.GetString("SportsLabel"));
                        //    else ContentContainer.Navigate(typeof(Home), sports);
                        //    break;
                        //case "news":
                        //    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "News", null, 1);
                        //    if (news == null)
                        //        news = await LoadFeed("mostPopular", "25", LanguageLoader.GetString("NewsLabel"));
                        //    else ContentContainer.Navigate(typeof(Home), news);
                        //    break;
                        //case "games":
                        //    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Games", null, 1);
                        //    if (games == null)
                        //        games = await LoadFeed("mostPopular", "20", LanguageLoader.GetString("GamesLabel"));
                        //    else ContentContainer.Navigate(typeof(Home), games);
                        //    break;
                        case "search":
                            FlyoutBase.ShowAttachedFlyout(item);
                            (sender as ListView).SelectedItem = null;
                            break;
                    }

                }
            }
            finally { }
        }        
    }
    public static class LinqExtensions
    {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> items)
        {
            return new ObservableCollection<T>(items);
        }
    }
    public class StandardFeeds
    {
        public StandardFeeds()
        {
            Feed = new object();
        }
        public string Name { get; set; }
        public ImageSource Image { get; set; }
        public object Feed { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.API.OAuth2;

namespace MiniTube_Mobile
{
    public static class Login
    {
        static Login()
        {
            App.Authenticated += App_Authenticated;
        }

        private static void App_Authenticated(object sender, AuthenticationEventArgs e)
        {
            RefreshToken = e.Token.refresh_token;

            if (method != null) method(e.Token);
        }

        static Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
        public static bool isLoggedIn
        {
            get
            {
                return localSettings.Values["RefAccessToken"] != null;
            }
        }
        public static UserInfo UserInfoData { get; set; }


        private static string _refToken;
        public static string RefreshToken
        {
            get
            {
                var t = localSettings.Values["RefAccessToken"];

                return t == null ? _refToken : t.ToString();

            }
            set { localSettings.Values["RefAccessToken"] = _refToken = value; }
        }
        public static void Logout()
        {
            localSettings.Values["RefAccessToken"] = null;
        }
        static Action<AccessToken> method;
        public async static Task GetToken(Action<AccessToken> p)
        {
            method = p;
            if (RefreshToken != null)
            {
                var tk = await(new Authentication()).GetAccessToken(RefreshToken, Authentication.GrantType.refresh_token);
                var user = await (new Profile()).GetUserInfo(tk);
                if (tk.refresh_token != null)
                    RefreshToken = tk.refresh_token;
                p(tk);
            }
            else
                App.GoogleAccountLogin();
        }
                
    }
    
}


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YouTube.API;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace MiniTube_Mobile
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ViewVideo : Page
    {
        public ViewVideo()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.DataContext = e.Parameter;

            LoadVideo((e.Parameter as YouTube.API.DataModel.Videos.Item).id);
        }
        private async void LoadVideo(string videoId)
        {
            try
            {
                //InitializeTransportControls();

                MyToolkit.Multimedia.YouTubeUri[] videourl = await MyToolkit.Multimedia.YouTube.GetUrisAsync(videoId);

                player.Source = videourl.Where(p => p.HasAudio == true && p.HasVideo == true).LastOrDefault().Uri;

                //DownloadMenu.Items.Clear();
                //playerResolutions.Items.Clear();
                //ResolutionsMenu.Items.Clear();
                //foreach (var r in videourl.Where(p => p.HasAudio == true))
                //{
                //    MenuFlyoutItem item = new MenuFlyoutItem();
                //    item.Text = r.VideoQuality.ToString().Replace("Quality", "").Replace("NotAvailable", "MP3"); ;
                //    item.Tag = r.Uri;
                //    item.Click += item_Click;

                //    MenuFlyoutItem item2 = new MenuFlyoutItem();
                //    item2.Text = r.VideoQuality.ToString().Replace("Quality", "").Replace("NotAvailable", "MP3"); ;
                //    item2.Tag = r.Uri;
                //    item2.Click += item2_Click;

                //    MenuFlyoutItem item3 = new MenuFlyoutItem();
                //    item3.Text = r.VideoQuality.ToString().Replace("Quality", "").Replace("NotAvailable", "MP3"); ;
                //    item3.Tag = r.Uri;
                //    item3.Click += item2_Click;


                //    DownloadMenu.Items.Add(item);
                //    playerResolutions.Items.Add(item2);
                //    ResolutionsMenu.Items.Add(item3);
                //}

                try
                {
                    YouTube.API.Parameters.SearchParameters parameters = new YouTube.API.Parameters.SearchParameters();
                    parameters.relatedToVideoId = videoId;
                    parameters.type = "video";
                    var result = await Search.List(parameters);
                    //try
                    //{
                    //    for (int i = 0; i < result.items.Count(); i++)
                    //    {
                    //        if (result.items[i].snippet.channelId == "UC")
                    //            result.items.RemoveAt(i);
                    //    }
                    //}
                    //catch { }
                    related.ItemsSource = result.items;
                }
                catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
            }
            catch
            {
                //ImageError.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }

        }

        private void l_ItemClick(object sender, ItemClickEventArgs e)
        {

        }
    }
    public class StringFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            // No format provided.
            if (parameter == null)
            {
                return value;
            }

            return String.Format((String)parameter, value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}

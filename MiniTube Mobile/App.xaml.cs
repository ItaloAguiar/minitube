﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Security.Authentication.Web;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using YouTube.API;
using YouTube.API.OAuth2;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace MiniTube_Mobile
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : Application
    {
        private TransitionCollection transitions;

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += this.OnSuspending;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                // TODO: change this value to a cache size that is appropriate for your application
                rootFrame.CacheSize = 1;

                // Set the default language
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    // TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // Removes the turnstile navigation for startup.
                if (rootFrame.ContentTransitions != null)
                {
                    this.transitions = new TransitionCollection();
                    foreach (var c in rootFrame.ContentTransitions)
                    {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;

                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(MainPage), e.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }

            // Ensure the current window is active
            Window.Current.Activate();
        }

        /// <summary>
        /// Restores the content transitions after the app has launched.
        /// </summary>
        /// <param name="sender">The object where the handler is attached.</param>
        /// <param name="e">Details about the navigation event.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            // TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        protected async override void OnActivated(IActivatedEventArgs args)
        {

            //CreateRootFrame();

            //// Restore the saved session state only when appropriate
            //if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
            //{
            //    try
            //    {
            //        await SuspensionManager.RestoreAsync();
            //    }
            //    catch (SuspensionManagerException)
            //    {
            //        //Something went wrong restoring state.
            //        //Assume there is no state and continue
            //    }
            //}

            //Check if this is a continuation
            var continuationEventArgs = args as IContinuationActivatedEventArgs;
            if (continuationEventArgs != null)
            {
                await ContinueWebAuthentication(args as Windows.ApplicationModel.Activation.WebAuthenticationBrokerContinuationEventArgs);
            }

            Window.Current.Activate();

        }
        public static event EventHandler<AuthenticationEventArgs> Authenticated;
        public static void GoogleAccountLogin()
        {
            string url = "https://accounts.google.com/o/oauth2/auth?redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code" +
                                "&client_id=" + Key.GetClientId() +
                                "&scope=" +
                                "https://www.googleapis.com/auth/youtube.force-ssl+" +
                                "https://www.googleapis.com/auth/userinfo.profile+" +
                                "https://www.googleapis.com/auth/userinfo.email+" +
                                "https://www.googleapis.com/auth/youtube";

            Uri startUri = new Uri(url);
            Uri endUri = new Uri("https://accounts.google.com/o/oauth2/approval?");
            WebAuthenticationBroker.AuthenticateAndContinue(startUri, endUri, null, WebAuthenticationOptions.UseTitle);
        }
        private async Task<AccessToken> ContinueWebAuthentication(Windows.ApplicationModel.Activation.WebAuthenticationBrokerContinuationEventArgs e)
        {
            Authentication a = new Authentication();
            if (e.WebAuthenticationResult.ResponseStatus == WebAuthenticationStatus.Success)
            {
                string token = e.WebAuthenticationResult.ResponseData.Replace("Success code=", "");
                AccessToken t = await a.GetAccessToken(token, Authentication.GrantType.authorization_code);
                if (Authenticated != null) Authenticated(this, new AuthenticationEventArgs(t));
                return t;
            }
            else if (e.WebAuthenticationResult.ResponseStatus == WebAuthenticationStatus.ErrorHttp)
            {
                throw new System.Net.Http.HttpRequestException("Failed to Connect to Login WebService");
            }
            else if (e.WebAuthenticationResult.ResponseStatus == WebAuthenticationStatus.UserCancel)
            {
                throw new OperationCanceledException("The user canceled the login attempt.");
            }
            else
            {
                throw new Exception("Unknown Error");
            }
        }
    }
    public class AuthenticationEventArgs : EventArgs
    {
        public AuthenticationEventArgs(AccessToken Token)
        {
            this.Token = Token;
        }
        public AccessToken Token { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization.Json;
using Windows.Security.Authentication.Web;

namespace YouTube.API.OAuth2
{
    public class Authentication
    {
        public async Task<AccessToken> Login(string Token = null)
        {
            string url = "https://accounts.google.com/o/oauth2/auth?redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code" +
                                "&client_id=" + Key.GetClientId() +
                                "&scope=" +
                                "https://www.googleapis.com/auth/youtube.force-ssl+"+
                                "https://www.googleapis.com/auth/userinfo.profile+"+
                                "https://www.googleapis.com/auth/userinfo.email+" +
                                "https://www.googleapis.com/auth/youtube";

            Uri startUri = new Uri(url);
            Uri endUri = new Uri("https://accounts.google.com/o/oauth2/approval?");


            if (Token != null)
            {
                return await GetAccessToken(Token, GrantType.refresh_token);
            }
            else
            {

                WebAuthenticationResult webAuthenticationResult = 
                    await WebAuthenticationBroker.AuthenticateAsync(WebAuthenticationOptions.UseTitle, startUri, endUri);


                if (webAuthenticationResult.ResponseStatus == WebAuthenticationStatus.Success)
                {
                    string token = webAuthenticationResult.ResponseData.Replace("Success code=", "");

                    AccessToken t = await GetAccessToken(token, GrantType.authorization_code);
                    return t;


                }
                else if (webAuthenticationResult.ResponseStatus == WebAuthenticationStatus.ErrorHttp)
                {
                    throw new System.Net.Http.HttpRequestException("Failed to Connect to Login WebService");
                }
                else if (webAuthenticationResult.ResponseStatus == WebAuthenticationStatus.UserCancel)
                {
                    throw new OperationCanceledException("The user canceled the login attempt.");
                }
                else
                {
                    throw new Exception("Unknown Error");
                }
            }
        }

        public enum GrantType
        {
            authorization_code,
            refresh_token
        }
        public async Task<AccessToken> GetAccessToken(string token, GrantType t)
        {
            List<KeyValuePair<string, string>> k = new List<KeyValuePair<string, string>>();
            k.Add(new KeyValuePair<string, string>(t == GrantType.authorization_code ? "code" : "refresh_token", token));
            k.Add(new KeyValuePair<string, string>("client_id", Key.GetClientId()));
            k.Add(new KeyValuePair<string, string>("client_secret", Key.GetSecretKey()));
            if (t == GrantType.authorization_code)
                k.Add(new KeyValuePair<string, string>("redirect_uri", "urn:ietf:wg:oauth:2.0:oob"));
            k.Add(new KeyValuePair<string, string>("grant_type", t.ToString()));

            var content = new FormUrlEncodedContent(k);
            HttpClient http = new System.Net.Http.HttpClient();
            var response = await http.PostAsync("https://accounts.google.com/o/oauth2/token", content);

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(AccessToken));
            return (AccessToken)ser.ReadObject(await response.Content.ReadAsStreamAsync());

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API
{
    public class ApiException:System.InvalidOperationException
    {
        public ApiException(string message)
            :base(message)
        {

        }
        public ApiException()
            : base()
        {

        }
        public ApiException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}

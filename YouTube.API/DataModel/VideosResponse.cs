﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.DataModel.Videos
{
    public class VideosResponse : Response
    {
        public Item[] items { get; set; }

        public override CommonItem[] GetItems()
        {
            return items;
        }
    }

    public class Item:CommonItem
    {
        public string id { get; set; }
        public Snippet snippet { get; set; }
        public Contentdetails contentDetails { get; set; }
        public Status status { get; set; }
        public Statistics statistics { get; set; }
        public Player player { get; set; }
        public Topicdetails topicDetails { get; set; }
    }

    public class Snippet:CommonSnippet
    {
        public string categoryId { get; set; }
        public string liveBroadcastContent { get; set; }
        public Localized localized { get; set; }
    }


    public class Localized
    {
        public string title { get; set; }
        public string description { get; set; }
    }

    public class Contentdetails
    {
        public string duration { get; set; }
        public string dimension { get; set; }
        public string definition { get; set; }
        public string caption { get; set; }
        public bool licensedContent { get; set; }
        public Regionrestriction regionRestriction { get; set; }
    }

    public class Regionrestriction
    {
        public string[] allowed { get; set; }
    }

    public class Status
    {
        public string uploadStatus { get; set; }
        public string privacyStatus { get; set; }
        public string license { get; set; }
        public bool embeddable { get; set; }
        public bool publicStatsViewable { get; set; }
    }

    public class Statistics
    {
        public long viewCount { get; set; }
        public long likeCount { get; set; }
        public long dislikeCount { get; set; }
        public long favoriteCount { get; set; }
        public long commentCount { get; set; }
        public long totalRatingCount
        {
            get
            {
                return likeCount + dislikeCount;
            }
        }
    }

    public class Player
    {
        public string embedHtml { get; set; }
    }

    public class Topicdetails
    {
        public string[] topicIds { get; set; }
        public string[] relevantTopicIds { get; set; }
    }



    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "toplevel", IsNullable = false)]
    public partial class SuggestionResponse
    {

        private toplevelCompleteSuggestion completeSuggestionField;

        /// <remarks/>
        public toplevelCompleteSuggestion CompleteSuggestion
        {
            get
            {
                return this.completeSuggestionField;
            }
            set
            {
                this.completeSuggestionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class toplevelCompleteSuggestion
    {

        private toplevelCompleteSuggestionSuggestion suggestionField;

        /// <remarks/>
        public toplevelCompleteSuggestionSuggestion suggestion
        {
            get
            {
                return this.suggestionField;
            }
            set
            {
                this.suggestionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class toplevelCompleteSuggestionSuggestion
    {

        private string dataField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }
    }


}

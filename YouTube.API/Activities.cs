﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.API.DataModel.Videos;
using YouTube.API.OAuth2;

namespace YouTube.API
{
    public class Activities
    {
        public void Insert()
        {

        }
        public async Task<VideosResponse> List(AccessToken token)
        {
            return await InternetHelper.GetAsync<VideosResponse>(token, "https://www.googleapis.com/youtube/v3/activities?part=snippet,statistics&home=true");
        }
        public void Delete()
        {

        }
    }    
}

﻿using DataBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections.ObjectModel;

namespace YouTube.API.Util
{
    public class IncrementalLoader<T1,T2> : IncrementalLoadingBase 
        where T1 : DataModel.Response
        where T2 : Parameters.Paramenter
    {
        protected bool _autoIncrement = true;
        public bool AutoIncrement
        {
            get { return _autoIncrement; }
            set { _autoIncrement = value; }
        }
        private Func<T2, Task<T1>> func;
        private T2 parameter;
        private string pageToken = string.Empty;
        private int MaxResults = 120, CurrentLoad = 0;
        public IncrementalLoader(Func<T2, Task<T1>> r, T2 p)
        {
            func = r;
            parameter = p;
        }
        protected override bool HasMoreItemsOverride()
        {
            return pageToken != null && CurrentLoad < MaxResults;
        }

        protected async override Task<ObservableCollection<object>> LoadMoreItemsOverrideAsync(CancellationToken c, uint count)
        {
            ObservableCollection<object> items = new ObservableCollection<object>();
            if (pageToken != null) parameter.pageToken = pageToken;
            var t = await func(parameter);
            pageToken = t.nextPageToken;
            CurrentLoad += t.GetItems().Count();
            items.AddRange(t.GetItems());
            return items;
        }
    }
    public static class LinqExtensions
    {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> items)
        {
            return new ObservableCollection<T>(items);
        }
        public static ObservableCollection<T> AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            foreach (var i in items) collection.Add(i);
            return collection;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using YouTube.API.OAuth2;

namespace YouTube.API
{
    public static class InternetHelper
    {
        public async static Task<T> RequestJsonAsync<T> (HttpRequestMessage httpMessage)
        {
            HttpClient http = new System.Net.Http.HttpClient();
            HttpResponseMessage response = await http.SendAsync(httpMessage);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new ApiException(await response.Content.ReadAsStringAsync());
            }
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            string r = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine(r);
            return (T)ser.ReadObject(await response.Content.ReadAsStreamAsync());
        }
        public async static Task<T> RequestXMLAsync<T>(HttpRequestMessage httpMessage)
        {
            HttpClient http = new System.Net.Http.HttpClient();
            HttpResponseMessage response = await http.SendAsync(httpMessage);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new ApiException(await response.Content.ReadAsStringAsync());
            }            
            XmlSerializer ser = new XmlSerializer(typeof(T));
            return (T)ser.Deserialize(await response.Content.ReadAsStreamAsync());
        }
        #region HTTP_GET
        public async static Task<T> GetAsync<T>(string url, object args)
        {
            return await RequestJsonAsync<T>(new HttpRequestMessage(HttpMethod.Get, string.Format("{0}{1}key={2}", url, args, Key.GetPublicKey())));
        }
        public async static Task<T> GetAsync<T>(AccessToken token, string url)
        {
            HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Get, url);
            msg.Headers.Add("Authorization", token.token_type + " " + token.access_token);

            return await RequestJsonAsync<T>(msg);
        }
        public async static Task<T> GetAsync<T>(AccessToken token, string url, object args)
        {
            return await GetAsync<T>(token, string.Format("{0}{1}key={2}", url, args, Key.GetPublicKey()));
        }
        #endregion

        #region HTTP_POST
        public async static Task<T> PostAsync<T>(AccessToken token, string url, string body = null, string contentType = "application/json")
        {
            HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, url);
            msg.Headers.Add("Authorization", token.token_type + " " + token.access_token);
            msg.Content = new StringContent(body);
            msg.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(contentType);

            return await RequestJsonAsync<T>(msg);
        }
        #endregion
        #region HTTP_DELETE
        public async static Task<T> DeleteAsync<T>(AccessToken token, string url)
        {
            HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Delete, url);
            msg.Headers.Add("Authorization", token.token_type + " " + token.access_token);
            return await RequestJsonAsync<T>(msg);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.Parameters
{
    public class CommentThreadsParameters: Paramenter
    {
        private string _part = "snippet";
        /// <summary>
        /// The part parameter specifies a comma-separated list of one or 
        /// more search resource properties that the API response will 
        /// include. Set the parameter value to snippet. 
        /// </summary>
        public string part
        {
            get
            {
                return _part;
            }
            set
            {
                _part = value;
            }
        }

        /// <summary>
        /// The channelId parameter indicates that the API response should only 
        /// contain resources created by the channel
        /// </summary>
        public string channelId { get; set; }

        private uint _maxResults = 25;
        /// <summary>
        /// The maxResults parameter specifies the maximum number of items that should 
        /// be returned in the result set. Acceptable values are 0 to 50, inclusive. 
        /// The default value is 5.
        /// </summary>
        public uint maxResults { get { return _maxResults; } set { _maxResults = value; } }

        /// <summary>
        /// The id parameter specifies a comma-separated list of comment thread IDs 
        /// for the resources that should be retrieved.
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// The allThreadsRelatedToChannelId parameter instructs the API to return 
        /// the comment threads of all videos of the channel and the channel 
        /// comments as well.
        /// </summary>
        public string allThreadsRelatedToChannelId { get; set; }

        /// <summary>
        /// Set this parameter to limit the returned comment threads to 
        /// a particular moderation state. Note: This parameter is not 
        /// supported for use in conjunction with the id parameter.
        /// </summary>
        public string moderationStatus { get; set; }

        /// <summary>
        /// The order parameter specifies the order in which the API 
        /// response should list comment threads. Valid values are: 
        /// - time - Comment threads are ordered by time. This is 
        /// the default behavior. - relevance - Comment threads are 
        /// ordered by relevance.Note: This parameter is not supported 
        /// for use in conjunction with the id parameter.
        /// </summary>
        public string order { get; set; }

        /// <summary>
        /// The searchTerms parameter instructs the API to limit the 
        /// returned comments to those which contain the specified 
        /// search terms. Note: This parameter is not supported for 
        /// use in conjunction with the id parameter.
        /// </summary>
        public string searchTerms { get; set; }

        /// <summary>
        /// Set this parameter's value to html or plainText to instruct 
        /// the API to return the comments left by users in html formatted 
        /// or in plain text.
        /// </summary>
        public string textFormat { get; set; }

        /// <summary>
        /// Selector specifying which fields to include in a partial response.
        /// </summary>
        public string fields { get; set; }

        /// <summary>
        /// The videoId parameter instructs the API to return the 
        /// comment threads for the video specified by the video id.
        /// </summary>
        public string videoId { get; set; }

        public override string ToString()
        {
            string c = "?";
            foreach (PropertyInfo p in this.GetType().GetRuntimeProperties())
            {
                if (p.GetValue(this, null) != null)
                {
                    c += string.Format("{0}={1}&", p.Name, p.GetValue(this, null));
                }
            }
            return c;
        }
    }
}

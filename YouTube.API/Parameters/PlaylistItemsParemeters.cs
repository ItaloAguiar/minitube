﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.Parameters
{
    public class PlaylistItemsParemeters:Paramenter
    {
        private string _part = "snippet";
        /// <summary>
        /// The part parameter specifies a comma-separated list of one or 
        /// more search resource properties that the API response will 
        /// include. Set the parameter value to snippet. 
        /// </summary>
        public string part
        {
            get
            {
                return _part;
            }
            set
            {
                _part = value;
            }
        }

        /// <summary>
        /// The id parameter specifies a comma-separated list of one or more unique playlist item IDs.
        /// </summary>
        public string id { get; set; }


        /// <summary>
        /// The videoId parameter specifies that the request should return only 
        /// the playlist items that contain the specified video.
        /// </summary>
        public string videoId { get; set; }

        /// <summary>
        /// This parameter can only be used in a properly authorized request. 
        /// Note: This parameter is intended exclusively for YouTube content partners.
        /// The onBehalfOfContentOwner parameter indicates that the request's 
        /// authorization credentials identify a YouTube CMS user who is acting on 
        /// behalf of the content owner specified in the parameter value. This parameter 
        /// is intended for YouTube content partners that own and manage many different 
        /// YouTube channels. It allows content owners to authenticate once and get access 
        /// to all their video and channel data, without having to provide authentication 
        /// credentials for each individual channel. The CMS account that the user 
        /// authenticates with must be linked to the specified YouTube content owner.
        /// </summary>
        public string onBehalfOfContentOwner { get; set; }


        /// <summary>
        /// Selector specifying which fields to include in a partial response.
        /// </summary>
        public string fields { get; set; }

        /// <summary>
        /// The playlistId parameter specifies the unique ID of the playlist for which 
        /// you want to retrieve playlist items. Note that even though this is an 
        /// optional parameter, every request to retrieve playlist items must specify a 
        /// value for either the id parameter or the playlistId parameter.
        /// </summary>
        public string playlistId { get; set; }

        private uint _maxResults = 25;
        /// <summary>
        /// The maxResults parameter specifies the maximum number of items that should 
        /// be returned in the result set. Acceptable values are 0 to 50, inclusive. 
        /// The default value is 25.
        /// </summary>
        public uint maxResults { get { return _maxResults; } set { _maxResults = value; } }

        public override string ToString()
        {
            string c = "?";
            foreach (PropertyInfo p in this.GetType().GetRuntimeProperties())
            {
                if (p.GetValue(this, null) != null)
                {
                    c += string.Format("{0}={1}&", p.Name, p.GetValue(this, null));
                }
            }
            return c;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using YouTube.API.Parameters;
using YouTube.API.DataModel.Videos;
using YouTube.API.OAuth2;
using System.Runtime.Serialization.Json;
using Windows.Data.Json;

namespace YouTube.API
{
    public static class Videos
    {
        private const string requestUri = "https://www.googleapis.com/youtube/v3/videos";
        public static void Insert()
        {

        }
        public static async Task<VideosResponse> List(VideosParameters parameters)
        {
            return await InternetHelper.GetAsync<VideosResponse>(requestUri, parameters);
        }
        public static async Task<VideosResponse> List(AccessToken token,VideosParameters parameters)
        {
            return await InternetHelper.GetAsync<VideosResponse>(token, requestUri, parameters);
        }
        public static void Delete()
        {

        }
        public static async Task<string> GetRating(AccessToken token, string videoId)
        {
            try
            {
                string url = string.Format("https://www.googleapis.com/youtube/v3/videos/getRating?id={0}",
                    videoId, Key.GetPublicKey());

                HttpClient http = new System.Net.Http.HttpClient();
                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Get, url);

                msg.Headers.Add("Authorization", token.token_type + " " + token.access_token);

                HttpResponseMessage response = await http.SendAsync(msg);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    string r = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine(r);
                    throw new ApiException(r);
                }
                JsonObject result = JsonValue.Parse(await response.Content.ReadAsStringAsync()).GetObject();
                return result["items"].GetArray()[0].GetObject()["rating"].GetString();
            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }
        public static async Task Rate(AccessToken token, string videoId, Rating rating)
        {
            try
            {
                string url = string.Format("https://www.googleapis.com/youtube/v3/videos/rate?id={0}&rating={1}&key={2}", 
                    videoId, rating.ToString(), Key.GetPublicKey());

                HttpClient http = new System.Net.Http.HttpClient();
                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, url);

                msg.Headers.Add("Authorization", token.token_type + " " + token.access_token);

                HttpResponseMessage response = await http.SendAsync(msg);
                if (!(response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.NoContent))
                {
                    string r = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine(r);
                    throw new ApiException(r);
                }
            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }
        public static void ReportAbuse()
        {

        }

        public enum Rating
        {
            like,dislike,none
        }
    }

}

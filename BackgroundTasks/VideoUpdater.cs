﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Foundation;
using Windows.UI.Notifications;
using YouTube.API;
using YouTube.API.OAuth2;

namespace BackgroundTasks
{
    public sealed class VideoUpdater : IBackgroundTask
    {
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            YouTube.API.Key key = new YouTube.API.Key();
            key.ClientId = "1092703638275-hp29l7oqaf1omps8bst06bjugju13seb.apps.googleusercontent.com";
            key.SecretKey = "1oNQuKoVsVx7sz10P1piCrLR";
            key.PublicKey = "AIzaSyAcadOlS_Cijs7v6PaWbM4CJ3i_3Zd8gXo";

            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();

            await GetNotifications();

            deferral.Complete();
        }
        private async Task GetNotifications()
        {
            try
            {
                var token = GetToken();

                if (token != null)
                {
                    System.Diagnostics.Debug.WriteLine(token.access_token);
                    YouTube.API.OAuth2.Profile p = new YouTube.API.OAuth2.Profile();
                    var rsp = p.GetUserId(token).AsAsyncOperation();
                    var channels = YouTube.API.Subscriptions.List(token, new YouTube.API.Parameters.SubscriptionsParameters()
                    {
                        channelId = rsp.AsTask().Result,
                        maxResults = 50
                    }).AsAsyncOperation();
                    string lastupdate = LastUpdate.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
                    foreach (var item in channels.AsTask().Result.items)
                    {
                        try
                        {
                            var videos = Search.List(new YouTube.API.Parameters.SearchParameters()
                            {
                                publishedAfter = lastupdate,
                                maxResults = 1,
                                channelId = item.snippet.resourceId.channelId                              
                                
                            }).AsAsyncOperation();
                            await Notify(videos.AsTask().Result);
                        }
                        catch(Exception e) { System.Diagnostics.Debug.WriteLine(e); }
                    }
                }   
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }
        private async Task Notify(YouTube.API.DataModel.Search.SearchResponse videos)
        {
            foreach (var video in videos.items)
            {

                var toast = ToastContent.ToastContentFactory.CreateToastImageAndText02();
                toast.TextHeading.Text = video.snippet.title;
                toast.TextBodyWrap.Text = video.snippet.channelTitle;
                toast.Image.Src = video.snippet.thumbnails.medium.url;
                toast.Image.Alt = video.snippet.title;
                toast.Launch = "video?id=" + video.id.videoId;

                ToastNotificationManager.CreateToastNotifier().Show(toast.CreateNotification());
            }
            await Task.Delay(8000);
        }

        Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

        private string _refToken;
        private string RefreshToken
        {
            get
            {
                var t = localSettings.Values["RefAccessToken"];

                return t == null ? _refToken : t.ToString();

            }
            set { localSettings.Values["RefAccessToken"] = _refToken = value; }
        }
        
        private DateTime LastUpdate
        {
            get
            {
                var t = localSettings.Values["LastToastUpdate"];
                localSettings.Values["LastToastUpdate"] = DateTime.Now.ToUniversalTime().ToString();
                return t == null ? DateTime.Today.ToUniversalTime() : DateTime.Parse(t.ToString());
            }
        }
        private AccessToken GetToken()
        {
            Authentication a = new Authentication();
            if (RefreshToken != null)
            {
                YouTube.API.OAuth2.AccessToken tk = null;
                try {
                    var rsp = a.Login(RefreshToken).AsAsyncOperation();
                    tk = rsp.AsTask().Result; 
                }
                catch(Exception e) { System.Diagnostics.Debug.WriteLine(e); }

                if (tk.refresh_token != null)
                    RefreshToken = tk.refresh_token;
                return tk;
            }
            else
            {
                return null;
            }
        }
    }
}

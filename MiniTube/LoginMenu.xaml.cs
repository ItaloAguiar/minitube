﻿using MiniTube;
using System.Collections.Generic;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using YouTube.API;
using YouTube.API.OAuth2;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MiniTube
{
    public sealed partial class LoginMenu : UserControl
    {
        public LoginMenu()
        {
            this.InitializeComponent();
            Login.LoggedIn += Login_LoggedIn;
            
        }
        ~LoginMenu()
        {
            Login.LoggedIn -= Login_LoggedIn;
        }


        void Login_LoggedIn(object sender, LoginEventArgs e)
        {
            SetUserInfoLayout(e.UserInfo);
            
        }
        private async void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await Login.GetToken();
            }
            catch { }
        }
        private void SetUserInfoLayout(UserInfo info)
        {
            LoginButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            if(info.name != null)
            Username.Text = info.name;
            if(info.picture != null)
            UserPicture.Source = new BitmapImage(new System.Uri(info.picture));
            userinfo = info;
        }
        UserInfo userinfo;
        //ProfileEntry Profile;
        private void MenuFlyoutItem_Click(object sender, RoutedEventArgs e)
        {
            Login.Logout();
            LoginButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }
        //Channel
        private async void MenuFlyoutItem_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                MainPage.GetInstance().IsLoading = true;
                List<StandardFeed> feeds = new List<StandardFeed>();
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Search", null, 1);
                ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                feeds.Add(new StandardFeed()
                {
                    Name = LanguageLoader.GetString("Uploads"),
                    /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                    Feed = (await YouTube.API.Search.List(await Login.GetToken(), new YouTube.API.Parameters.SearchParameters()
                    {
                        maxResults = 50,
                        type = "video",
                        forMine = "true"

                    }
                    )).items.ToObservableCollection()
                });

                MainPage.GetInstance().SetContent(typeof(Home), feeds);
            }
            finally
            {
                MainPage.GetInstance().IsLoading = false;
            }          
        }
        public string ChannelId { get; set; }
        
        private async void MenuFlyoutItem_Click_3(object sender, RoutedEventArgs e)
        {
            try
            {
                MainPage.GetInstance().IsLoading = true;
                if (ChannelId == null)
                {
                    YouTube.API.OAuth2.Profile p = new Profile();
                    ChannelId = await p.GetUserId(await Login.GetToken());
                }
                var channels = await YouTube.API.Subscriptions.List(await Login.GetToken(), new YouTube.API.Parameters.SubscriptionsParameters()
                {
                    channelId = ChannelId,
                    maxResults = 50
                });
                MainPage.GetInstance().SetContent(typeof(SubscriptionsPage), channels.items);
            }
            finally
            {
                MainPage.GetInstance().IsLoading = false;
            }
        } 
        
    }    
}

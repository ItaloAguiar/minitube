﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Store;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media;
using Windows.System.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using YouTube.API;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniTube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ViewPlaylist : Page
    {
        Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

        public ViewPlaylist()
        {
            this.InitializeComponent();            
            GoogleAnalytics.EasyTracker.GetTracker().SendView("ViewPlaylist");

            if (App.IsAdEnabled)
            {
                AdContainer.Width = 160;
                ad.IsEnabled = true;
            }
        }
        YouTube.API.DataModel.Videos.Item Video;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            var item = e.Parameter;
            Load(item);
        }
        
        private int currentIndex = 0, playlistLenght = 0;
        private void Load(object item)
        {
            var playlist = item as List<YouTube.API.DataModel.PlaylistItems.Item>;
            itemListView.ItemsSource = playlist;
            itemListView.SelectedIndex = 0;
            playlistLenght = playlist.Count;
        }
        private async void LoadComments(string videoId)
        {
            try
            {
                var cmts = await YouTube.API.CommentThreads.List(new YouTube.API.Parameters.CommentThreadsParameters()
                {
                    videoId = videoId,
                    textFormat = "plainText",
                    maxResults = 30
                });
                Comments.Items.Clear();
                foreach (var c in cmts.items)
                {
                    Comments.Items.Add(c);
                }
            }
            catch { }
        }
        private async void LoadVideo(string videoId)
        {
            try
            {
                InitializeTransportControls();

                MyToolkit.Multimedia.YouTubeUri[] videourl = await MyToolkit.Multimedia.YouTube.GetUrisAsync(videoId);

                

                player.Source = videourl.Where(p => p.HasAudio == true && p.HasVideo == true).LastOrDefault().Uri;

                DownloadMenu.Items.Clear();
                playerResolutions.Items.Clear();
                ResolutionsMenu.Items.Clear();
                foreach (var r in videourl.Where(p => p.HasAudio == true))
                {
                    MenuFlyoutItem item = new MenuFlyoutItem();
                    item.Text = r.VideoQuality.ToString().Replace("Quality", "").Replace("NotAvailable", "MP3"); ;
                    item.Tag = r.Uri;
                    item.Click += item_Click;

                    MenuFlyoutItem item2 = new MenuFlyoutItem();
                    item2.Text = r.VideoQuality.ToString().Replace("Quality", "").Replace("NotAvailable", "MP3"); ;
                    item2.Tag = r.Uri;
                    item2.Click += item2_Click;

                    MenuFlyoutItem item3 = new MenuFlyoutItem();
                    item3.Text = r.VideoQuality.ToString().Replace("Quality", "").Replace("NotAvailable", "MP3"); ;
                    item3.Tag = r.Uri;
                    item3.Click += item2_Click;


                    DownloadMenu.Items.Add(item);
                    playerResolutions.Items.Add(item2);
                    ResolutionsMenu.Items.Add(item3);
                }
            }
            catch 
            {
                ImageError.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            
        }

        private void item2_Click(object sender, RoutedEventArgs e)
        {
            MenuFlyoutItem item = sender as MenuFlyoutItem;
            TimeSpan ts = player.Position;
            player.Source = item.Tag as Uri;
            player.MediaOpened += (send, args) =>
            {
                player.Position = ts.Subtract(new TimeSpan(0, 0, 1));
            };
        }

        void item_Click(object sender, RoutedEventArgs e)
        {
            MenuFlyoutItem item = sender as MenuFlyoutItem;
            StaticData.AddDownload(item.Tag as Uri, Video.snippet.title, Video.snippet.thumbnails.medium.url, item.Text == "MP3"?"mp3": "mp4");
            StaticData.ShowDownloadPanel();
            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Download", null, 1);
        }
        private async void GetRating(string videoId)
        {
            try
            {
                var token = await Login.GetTokenIfLoggedIn();
                if (token != null)
                {
                    string rating = await YouTube.API.Videos.GetRating(token, videoId);
                    if (rating != "none")
                    {
                        DislikeBtn.IsDisliked = !(LikeBtn.IsLiked = (rating) == "like");
                    }
                }
            }
            catch
            {

            }
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack) Frame.GoBack();
        }

  

       
        private async void PostComment_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var token = await Login.GetToken();

                var response = await CommentThreads.Insert(token, CommentBox.Text, Video.id);
                Comments.Items.Insert(0, response);
                CommentBox.Text = string.Empty;
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Comment", null, 1);
            }
            catch { }

        }
        private Size _previousVideoContainerSize = new Size();
        private void player_FullScreenChanged(object sender, YouTube.UI.FullScreenEventArgs e)
        {
            if (e.IsFullScreen)
            {
                _previousVideoContainerSize.Width = (sender as YouTube.UI.Player).ActualWidth;
                _previousVideoContainerSize.Height = (sender as YouTube.UI.Player).ActualHeight;


                (sender as YouTube.UI.Player).Width = Window.Current.Bounds.Width;
                (sender as YouTube.UI.Player).Height = Window.Current.Bounds.Height;
            }
            else
            {
                (sender as YouTube.UI.Player).Width = _previousVideoContainerSize.Width;
                (sender as YouTube.UI.Player).Height = _previousVideoContainerSize.Height;
            }
        }

        private async void SubscribeButton_RequestForSubscribe(object sender, YouTube.UI.SubscribeEventArgs e)
        {
            if (SubscribeBtn.IsSubscribed)
            {
                try
                {
                    var token = await Login.GetToken();
                    await YouTube.API.Subscriptions.Delete(token, e.SubscriptionID);
                    e.Success = false;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Channel", "Unsubscribe", null, 1);
                }
                catch
                {
                    SubscribeBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                }
            }
            else
            {
                try
                {
                    var token = await Login.GetToken();
                    var response = await Subscriptions.Insert(token, e.ChannelID);
                    System.Diagnostics.Debug.WriteLine(response);
                    e.Success = true;
                    e.SubscriptionID = response.id;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Channel", "Subscribe", null, 1);
                }
                catch (ApiException ex)
                {
                    if (ex.Message.Contains("subscriptionDuplicate")) e.Success = true;
                    else
                    {
                        e.Success = true;
                        e.Success = false;
                    }
                }
            }
        }

        private async void LikeBtn_RequestForLike(object sender, YouTube.UI.LikeEventArgs e)
        {
            try
            {
                if (LikeBtn.IsLiked)
                {
                    var token = await Login.GetToken();
                    await YouTube.API.Videos.Rate(token, Video.id, Videos.Rating.none);
                    e.Success = false;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Cancel Like", null, 1);
                }
                else
                {
                    var token = await Login.GetToken();
                    await YouTube.API.Videos.Rate(token, Video.id, Videos.Rating.like);
                    e.Success = true;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Like", null, 1);
                }
            }
            catch { }

        }

        private async void DislikeBtn_RequestForDislike(object sender, YouTube.UI.DislikeEventArgs e)
        {
            try
            {
                if (DislikeBtn.IsDisliked)
                { 
                    var token = await Login.GetToken();
                    await YouTube.API.Videos.Rate(token, Video.id, Videos.Rating.none);
                    e.Success = false;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Cancel Dislike", null, 1);
                }
                else
                {
                    var token = await Login.GetToken();
                    await YouTube.API.Videos.Rate(token, Video.id, Videos.Rating.dislike);
                    e.Success = true;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Dislike", null, 1);
                }
            }
            catch { }
        }
        //download
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            StaticData.ShowDownloadPanel();
        }

        private async void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(
                new Uri(string.Format("https://www.youtube.com/watch?v={0}&feature=youtube_gdata_player", Video.id))
            );
        }

        private void player_MediaEnded(object sender, RoutedEventArgs e)
        {
            if(++currentIndex < playlistLenght)
            {
                itemListView.SelectedIndex = currentIndex;
            }
            systemControls.DisplayUpdater.ClearAll();
        }

        private void player_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            switch (player.CurrentState)
            {
                case MediaElementState.Playing:
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Playing;
                    break;
                case MediaElementState.Paused:
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Paused;
                    break;
                case MediaElementState.Stopped:
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Stopped;
                    dispRequest.RequestRelease();
                    break;
                case MediaElementState.Closed:
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Closed;
                    break;
                default:
                    break;
            }
        }
        #region TransportControls
        SystemMediaTransportControls systemControls;

        void InitializeTransportControls()
        {
            // Hook up app to system transport controls.
            systemControls = SystemMediaTransportControls.GetForCurrentView();
            systemControls.ButtonPressed += SystemControls_ButtonPressed;

            // Register to handle the following system transpot control buttons.
            systemControls.IsPlayEnabled = true;
            systemControls.IsPauseEnabled = true;
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            systemControls.DisplayUpdater.ClearAll();
            systemControls.ButtonPressed -= SystemControls_ButtonPressed;
        }
        void SystemControls_ButtonPressed(SystemMediaTransportControls sender, SystemMediaTransportControlsButtonPressedEventArgs args)
        {
            switch (args.Button)
            {
                case SystemMediaTransportControlsButton.Play:
                    PlayMedia();
                    break;
                case SystemMediaTransportControlsButton.Pause:
                    PauseMedia();
                    break;
                case SystemMediaTransportControlsButton.Next:
                    GotoNextVideo();
                    break;
                case SystemMediaTransportControlsButton.Previous:
                    GotoPrevVideo();
                    break;
                default:
                    break;
            }
        }
        async void GotoNextVideo()
        {
            ++currentIndex;
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (currentIndex < 0) currentIndex = 0;
                if (currentIndex < playlistLenght)
                {
                    itemListView.SelectedIndex = currentIndex;
                }
            });
        }
        async void GotoPrevVideo()
        {
            --currentIndex;
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (currentIndex > -1)
                {
                    itemListView.SelectedIndex = currentIndex;
                }
            });
        }
        async void PlayMedia()
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                player.Play();
            });
        }

        async void PauseMedia()
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                player.Pause();
            });
        }
        DisplayRequest dispRequest = new DisplayRequest();
        private void player_MediaOpened(object sender, RoutedEventArgs e)
        {   
            systemControls.DisplayUpdater.Type = MediaPlaybackType.Video;
            systemControls.DisplayUpdater.VideoProperties.Title = Video.snippet.title;
            systemControls.DisplayUpdater.VideoProperties.Subtitle = Video.snippet.channelTitle;
            systemControls.IsNextEnabled = (currentIndex + 1) < playlistLenght;
            systemControls.IsPreviousEnabled = currentIndex > 0;
            systemControls.DisplayUpdater.Update();
            dispRequest.RequestActive();
            ImageError.Visibility = Visibility.Collapsed;
        }
        #endregion



        private void player_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //if (player.CurrentState == MediaElementState.Playing) player.Pause();
            //else if(player.CurrentState == MediaElementState.Paused) player.Play();
        }

        private async void itemListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var video = itemListView.SelectedItem as YouTube.API.DataModel.PlaylistItems.Item;
            LoadVideo(video.snippet.resourceId.videoId);
            GetRating(video.snippet.resourceId.videoId);

            LoadComments(video.snippet.resourceId.videoId);

            currentIndex = itemListView.SelectedIndex;

            this.DataContext = video;

            try
            {
                this.DataContext = Video = (await Videos.List(new YouTube.API.Parameters.VideosParameters()
                {
                    id = video.snippet.resourceId.videoId,
                    part = "snippet,statistics"
                })).items[0];
            }
            catch { }
            
        }
        private async void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var currentLicense = CurrentApp.LicenseInformation;
            try
            {
                var results = await CurrentApp.RequestProductPurchaseAsync("MTRA001");
                //Check the license state to determine if the in-app purchase was successful.

                if (currentLicense.ProductLicenses["MTRA001"].IsActive ||
                    results.Status == ProductPurchaseStatus.AlreadyPurchased ||
                    results.Status == ProductPurchaseStatus.Succeeded)
                {
                    AdContainer.Width = 0;
                    ad.IsEnabled = false;
                }

            }
            catch (Exception)
            {
            }
        }

        private void ad_AdRefreshed(object sender, RoutedEventArgs e)
        {
            removeAdBtn.Visibility = Visibility.Visible;
        }

        private void ad_ErrorOccurred(object sender, Microsoft.Advertising.WinRT.UI.AdErrorEventArgs e)
        {
            removeAdBtn.Visibility = Visibility.Collapsed;
        }
    }
}

﻿using Microsoft.Advertising.WinRT.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;

namespace MiniTube
{
    public class AdMediator:ContentControl
    {
        public AdMediator():base()
        {
            pubcenter = new AdControl();
            timer = new DispatcherTimer();

            pubcenter.AdUnitId = "11398173";
            pubcenter.ApplicationId = "7e32a26e-2d3d-47d3-b096-82419555c614";
            

            pubcenter.ErrorOccurred += (sender, args) =>
            {
                //SetSmaato();
                System.Diagnostics.Debug.WriteLine(args.Error + Environment.NewLine + args.ErrorCode);
            };
            pubcenter.AdRefreshed += (sender, args) =>
            {
            };

           

            //timer.Interval = new TimeSpan(0, 3, 0);
            //timer.Tick += (sender, args) =>
            //{
            //    System.Diagnostics.Debug.WriteLine("Ad Refreshed");
            //    SetPubCenter();
            //};
            //timer.Start();

            SetPubCenter();
        }
        private AdControl pubcenter;
        private DispatcherTimer timer;

        private void SetPubCenter()
        {
            pubcenter.IsEnabled = true;
            this.Content = pubcenter;
            System.Diagnostics.Debug.WriteLine("PubCenter Inserted");
        }
          
    }
}

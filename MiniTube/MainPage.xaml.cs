﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YouTube.API;
using YouTube.API.Parameters;
using YouTube.API.DataModel.Search;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniTube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            GoogleAnalytics.EasyTracker.GetTracker().SendView("Home");
                                
        }
        LocalVideoParamenter parameter;
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter.ToString().StartsWith("video"))
            {
                try
                {
                    IsLoading = true;
                    parameter = new LocalVideoParamenter();
                    var video = await Videos.List(new YouTube.API.Parameters.VideosParameters()
                    {
                        part = "snippet,statistics",
                        id = e.Parameter.ToString().Split('=').LastOrDefault()
                    });
                    parameter.Video = video.items[0];
                }
                finally { IsLoading = false; }
            }
            this.RegisterBackgroundTask();
        }


        private async void RegisterBackgroundTask()
        {
            var backgroundAccessStatus = await BackgroundExecutionManager.RequestAccessAsync();
            if (backgroundAccessStatus == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity ||
                backgroundAccessStatus == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity)
            {
                foreach (var task in BackgroundTaskRegistration.AllTasks)
                {
                    if (task.Value.Name == taskName)
                    {
                        task.Value.Unregister(true);
                    }
                }

                BackgroundTaskBuilder taskBuilder = new BackgroundTaskBuilder();
                taskBuilder.AddCondition(new SystemCondition(SystemConditionType.InternetAvailable));
                taskBuilder.Name = taskName;
                taskBuilder.TaskEntryPoint = taskEntryPoint;
                taskBuilder.SetTrigger(new TimeTrigger(360, false));
                var registration = taskBuilder.Register();
            }
        }

        private const string taskName = "VideoUpdater";
        private const string taskEntryPoint = "BackgroundTasks.VideoUpdater";

        public async Task<List<StandardFeed>> LoadFeed(string chart = "mostPopular", string categoryId = null, string title = "Popular on YouTube")
        {
            var token = await Login.GetTokenIfLoggedIn();

            List<StandardFeed> feeds = new List<StandardFeed>();

            var feed = (await Videos.List(new YouTube.API.Parameters.VideosParameters()
                {
                    chart = chart,
                    part = "snippet,statistics",
                    regionCode = System.Globalization.RegionInfo.CurrentRegion.Name,
                    maxResults = 50,
                    videoCategoryId = categoryId                    
                }
            )).items.ToObservableCollection();
            feeds.Add(new StandardFeed()
            {
                Name = title,
                /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                Feed = feed
            });

            //if(categoryId == null && token != null)
            //{
            //    var f = await (new YouTube.API.Activities()).List(token);
            //    feeds.Add(new StandardFeeds()
            //    {
            //        Name = "Vídeos recomendados",
            //        /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
            //        Feed = f.items.ToObservableCollection()
            //    });
            //}

            ContentContainer.Navigate(typeof(Home),feeds);

            await Task.Delay(1000);

            if(parameter != null)
                ContentContainer.Navigate(typeof(ViewVideo), parameter);

            parameter = null;
            return feeds;            
        }
        public async Task<List<StandardFeed>> LoadLiked(string title = "Liked Videos")
        {
            List<StandardFeed> feeds = new List<StandardFeed>();

            var feed = (await Videos.List(await Login.GetToken(), new YouTube.API.Parameters.VideosParameters()
            {
                part = "snippet,statistics",
                maxResults = 50,
                myRating = "like"
            }
            )).items.ToObservableCollection();
            feeds.Add(new StandardFeed()
            {
                Name = title,
                /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                Feed = feed
            });

            ContentContainer.Navigate(typeof(Home), feeds);

            await Task.Delay(1000);

            if (parameter != null)
                ContentContainer.Navigate(typeof(ViewVideo), parameter);

            parameter = null;
            return feeds;
        }
        public async Task<List<StandardFeed>> LoadSubscriptionFeed()
        {
            List<StandardFeed> feeds = new List<StandardFeed>();
            try
            {
                var token = await Login.GetToken();

                IsLoading = true;
                YouTube.API.OAuth2.Profile p = new YouTube.API.OAuth2.Profile();
                var channels = await YouTube.API.Subscriptions.List(token, new YouTube.API.Parameters.SubscriptionsParameters()
                {
                    channelId = await p.GetUserId(token),
                    maxResults = 50
                });
                foreach (var item in channels.items)
                {
                    try
                    {
                        feeds.Add(new StandardFeed()
                        {
                            Name = item.snippet.title,
                            /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                            Feed = (await YouTube.API.Search.List(new YouTube.API.Parameters.SearchParameters()
                            {
                                channelId = item.snippet.resourceId.channelId,
                                order = YouTube.API.Parameters.SearchParameters.OrderType.date,
                                maxResults = 14,
                                type = "video"

                            }
                        )).items.ToObservableCollection()
                        });
                    }
                    catch { }
                }
                SetContent(typeof(Home), feeds);
            }
            finally
            {
                IsLoading = false;
            }
            
            return feeds;
        }
        public void Search(string text)
        {
            try
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Search", text, 1);
                ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                StandardFeed feed = (new StandardFeed()
                {
                    Name = string.Format(LanguageLoader.GetString("SearchResults"), text),
                    /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                    Feed = new YouTube.API.Util.IncrementalLoader<SearchResponse, SearchParameters> (YouTube.API.Search.List, new SearchParameters()
                    {
                        q = text,
                        maxResults = 40,
                        type = "video,playlist"//,
                        //safeSearch = YouTube.API.Parameters.SearchParameters.SafeSearch.strict                        

                    })
                });

                ContentContainer.Navigate(typeof(Search),feed);
            }
            catch { }

        }
        string previousQuery = string.Empty;
        private async void SearchBox_SuggestionsRequested(SearchBox sender, SearchBoxSuggestionsRequestedEventArgs e)
        {
            var deferral = e.Request.GetDeferral();
            try
            {
                string queryText = e.QueryText;
                if (!string.IsNullOrEmpty(queryText) && previousQuery != queryText)
                {
                    Windows.ApplicationModel.Search.SearchSuggestionCollection suggestionCollection = e.Request.SearchSuggestionCollection;
                    var suggestions = await YouTube.API.Search.Suggest(queryText);
                    foreach (string suggestion in suggestions)
                    {
                        if (suggestion.StartsWith(queryText, StringComparison.CurrentCultureIgnoreCase))
                        {
                            suggestionCollection.AppendQuerySuggestion(suggestion);
                        }
                    }
                    previousQuery = queryText;
                }
            }
            finally
            {
                deferral.Complete();
            }
        }

        private void SearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            Search(args.QueryText);
        }
        public void SetContent(Type u, object parameter)
        {
            ContentContainer.Navigate(u, parameter);            
        }
        public bool IsLoading
        {
            set
            {
                try
                {
                    progress.IsIndeterminate = value;
                }
                catch { }
            }
            get { return progress.IsIndeterminate; }
        }
        public static MainPage GetInstance()
        {
            var frame = (Frame)Window.Current.Content;
            return (MainPage)frame.Content;
        }
        public Frame GetContainer()
        {
            return ContentContainer;
        }
        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            if (ContentContainer.CanGoBack) ContentContainer.GoBack();
        }
        object home, news, sports, movies, games, music,subscriptions, like;
        private async void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (sender as ListView).SelectedItem as ListViewItem;
            IsLoading = true;
            try
            {
                if (item != null)
                {                    
                    ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                    switch (item.Tag.ToString())
                    {
                        case "home":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Popular", null, 1);
                            if (home == null)
                                home = await LoadFeed("mostPopular", null, LanguageLoader.GetString("PopularLabel"));
                            else
                                ContentContainer.Navigate(typeof(Home),home);
                            break;
                        case "subscriptions":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Subscriptions Feed", null, 1);
                            if (subscriptions == null)
                            {
                                subscriptions = await LoadSubscriptionFeed();
                                try
                                {
                                    foreach (var i in (List<StandardFeed>)subscriptions)
                                        ((List<StandardFeed>)home).Add(i);
                                }
                                catch { }
                            }
                            else ContentContainer.Navigate(typeof(Home), subscriptions);
                            break;
                        case "liked":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Liked Videos", null, 1);
                            if (like == null)
                                like = await LoadLiked();
                            else ContentContainer.Navigate(typeof(Home), like);
                            break;
                        case "music":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Music", null, 1);
                            if (music == null)
                                music = await LoadFeed("mostPopular", "10", LanguageLoader.GetString("MusicLabel"));
                            else ContentContainer.Navigate(typeof(Home),music);
                            break;
                        case "sports":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Sports", null, 1);
                            if (sports == null)
                                sports = await LoadFeed("mostPopular", "17", LanguageLoader.GetString("SportsLabel"));
                            else ContentContainer.Navigate(typeof(Home),sports);
                            break;
                        case "movies":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Movies", null, 1);
                            if (movies == null)
                                movies = await LoadFeed("mostPopular", "1", LanguageLoader.GetString("MoviesLabel"));
                            else ContentContainer.Navigate(typeof(Home),movies);
                            break;
                        case "news":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "News", null, 1);
                            if (news == null)
                                news = await LoadFeed("mostPopular", "25", LanguageLoader.GetString("NewsLabel"));
                            else ContentContainer.Navigate(typeof(Home),news);
                            break;
                        case "games":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Games", null, 1);
                            if (games == null)
                                games = await LoadFeed("mostPopular", "20", LanguageLoader.GetString("GamesLabel"));
                            else ContentContainer.Navigate(typeof(Home),games);
                            break;
                        case "settings":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Settings", null, 1);
                            SettingsPane.Show();
                            (sender as ListView).SelectedItem = null;
                            break;
                        case "search":
                            FlyoutBase.ShowAttachedFlyout(item);                            
                            (sender as ListView).SelectedItem = null;
                            break;
                    }
                    
                }
            }
            catch { }
            finally
            {
                IsLoading = false;
            }
        }
    }
    public static class LinqExtensions
    {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> items)
        {
            return new ObservableCollection<T>(items);
        }
    }
    public class LocalVideoParamenter
    {
        public object Video { get; set; }
        public object RelatedVideos { get; set; }
        public object SearchVideo { get; set; }
        public object Playlist { get; set; }
    }
    public class StandardFeed
    {
        public StandardFeed()
        {
            Feed = new object();
        }
        public string Name { get; set; }
        public ImageSource Image { get; set; }
        public object Feed { get; set; }
    }
}

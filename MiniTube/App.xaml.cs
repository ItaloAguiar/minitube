﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Store;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YouTube.API;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace MiniTube
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
            this.UnhandledException += App_UnhandledException;

            //Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = "";


            //Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();
            ////Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();

            //Windows.ApplicationModel.Resources.Core.ResourceManager.Current.DefaultContext.Reset();

            
        }
        static Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
        public static bool IsAdEnabled
        {
            get
            {
                var currentLicense = CurrentApp.LicenseInformation;
                return (localSettings.Values["AdEnabled"] == null)||!currentLicense.ProductLicenses["MTRA001"].IsActive;
            }
            private set
            {
                localSettings.Values["AdEnabled"] = value.ToString();
            }
        }
        void App_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Exceção: {0}", e.Exception.ToString());
            GoogleAnalytics.EasyTracker.GetTracker().SendException(e.Exception.ToString(), true);
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected async override void OnLaunched(LaunchActivatedEventArgs e)
        {

#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
               // this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif
            SettingsPane.GetForCurrentView().CommandsRequested += App_CommandsRequested;
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                // Set the default language
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];                

                rootFrame.NavigationFailed += OnNavigationFailed;

                SuspensionManager.RegisterFrame(rootFrame, "appFrame");


                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                    try
                    {
                        await SuspensionManager.RestoreAsync();
                    }
                    catch { }
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                rootFrame.Navigate(typeof(MainPage), e.Arguments);
            }
            else
            {
                if (e.Arguments.ToString().StartsWith("video"))
                {
                    try
                    {
                        MainPage.GetInstance().IsLoading = true;
                        LocalVideoParamenter p = new LocalVideoParamenter();
                        var video = await Videos.List(new YouTube.API.Parameters.VideosParameters()
                        {
                            part = "snippet,statistics",
                            id = e.Arguments.ToString().Split('=').LastOrDefault()
                        });
                        p.Video = video.items[0];
                        MainPage.GetInstance().GetContainer().Navigate(typeof(ViewVideo), p);
                    }
                    finally { MainPage.GetInstance().IsLoading = false; }
                }
            }
            // Ensure the current window is active
            Window.Current.Activate();
        }

        private void App_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            try
            {
                ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                args.Request.ApplicationCommands.Add(
                    new SettingsCommand("UID1", LanguageLoader.GetString("About"), new UICommandInvokedHandler(async (cmd) =>
                    {
                        await Windows.System.Launcher.LaunchUriAsync(new Uri("http://www.epicapps.com.br/"));
                    }))
                );
                args.Request.ApplicationCommands.Add(
                    new SettingsCommand("UID2", LanguageLoader.GetString("Privacy"), new UICommandInvokedHandler(async (cmd) =>
                    {
                        await Windows.System.Launcher.LaunchUriAsync(new Uri("http://www.epicapps.com.br/privacy/"));
                    }))
                );
                args.Request.ApplicationCommands.Add(
                    new SettingsCommand("UID3", LanguageLoader.GetString("Rate"), new UICommandInvokedHandler(async (cmd) =>
                    {
                        await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:reviewapp?appid=aa870db0-c7b9-4d09-bb06-2d5da0f48005"));
                    }))
                );
                args.Request.ApplicationCommands.Add(
                    new SettingsCommand("UID5", LanguageLoader.GetString("DownloadManager"), new UICommandInvokedHandler((cmd) =>
                    {
                        StaticData.ShowDownloadPanel();
                    }))
                );
            }
            catch { }
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            try
            {
                var deferral = e.SuspendingOperation.GetDeferral();
                //TODO: Save application state and stop any background activity
                await SuspensionManager.SaveAsync();
                deferral.Complete();
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }
    }
}

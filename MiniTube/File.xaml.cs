﻿using NotificationsExtensions.ToastContent;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MiniTube
{
    public sealed partial class File : UserControl
    {
        public File(System.Uri Url, string name, string imageUrl, string ext)
        {
            this.InitializeComponent();
            this.Url = Url;
            thumb.Source = new BitmapImage(new Uri(imageUrl));
            StartDownload(Url,name,ext);
        }
        ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
        public System.Uri Url { get; set; }


        #region Download
        StorageFile filedownloaded;
        BackgroundDownloader downloader = new BackgroundDownloader();
        DownloadOperation download;
        bool finished = false;
        private async void StartDownload(System.Uri uri, string name, string ext)
        {
            try
            {

                await Task.Delay(500);
                // set download URI
                // get destination file
                var picker = new FileSavePicker();
                // set allowed extensions
                picker.SuggestedFileName = name;
                picker.SuggestedStartLocation = PickerLocationId.VideosLibrary;
                picker.FileTypeChoices.Add(ext, new List<string> {"."+ ext });
                var file = await picker.PickSaveFileAsync();
                filedownloaded = file;
                filename.Text = file.DisplayName;

                if (file != null)
                {
                    ToastNotification success = CreateToastText02(LanguageLoader.GetString("DownloadCompleted"), LanguageLoader.GetString("DownloadCompletedMessage"));
                    success.Activated += toast_Activated;
                    downloader.SuccessToastNotification = success;

                    downloader.FailureToastNotification = CreateToastText02(LanguageLoader.GetString("DownloadFailed"), LanguageLoader.GetString("DownloadFailedMessage"));
                                        
                    // create a background download
                    download = downloader.CreateDownload(uri, file);
                    
                    // create progress object
                    var progress = new Progress<DownloadOperation>();                    
                    // attach an event handler to get notified on progress
                    progress.ProgressChanged += (o, operation) =>
                    {
                        // use the progress info in Progress.BytesReceived and Progress.TotalBytesToReceive
                        DownloadProgress(operation);
                    };
                    await download.StartAsync().AsTask(progress);
                    

                    ProgressBar1.Value = 0;
                    finished = true;
                    btnCancel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    btnPause.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    btnOpen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    StaticData.ShowDownloadPanel();
                }
            }
            catch (Exception e)
            {
                ShowMessage(LanguageLoader.GetString("DownloadFailed"));
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }
        private void DownloadProgress(DownloadOperation download)
        {            
            double percent = 100;
            if (download.Progress.TotalBytesToReceive > 0)
            {
                percent = download.Progress.BytesReceived * 100 / download.Progress.TotalBytesToReceive;
            }
            ProgressBar1.Value = percent;
            statistics.Text = string.Format("{0}/{1}", download.Progress.BytesReceived.ToFileSize(), download.Progress.TotalBytesToReceive.ToFileSize());
        }

        private ToastNotification CreateToastText02(string title, string content)
        {
            IToastText02 toastContent = ToastContentFactory.CreateToastText02();

            // Set the launch activation context parameter on the toast.
            // The context can be recovered through the app Activation event
            toastContent.Launch = "Context123";

            toastContent.TextHeading.Text = title;
            toastContent.TextBodyWrap.Text = content;

            return toastContent.CreateNotification();
        }
        private async void toast_Activated(ToastNotification sender, object args)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                LaunchFileAsync();
            });
        }
        private async void LaunchFileAsync()
        {
            try
            {
                var options = new Windows.System.LauncherOptions();
                options.DisplayApplicationPicker = true;
                await Windows.System.Launcher.LaunchFileAsync(filedownloaded, options);
            }
            catch
            {
                ShowMessage(LanguageLoader.GetString("FileOpenError"));
            }
        }
        private async void ShowMessage(string message)
        {
            try
            {
                MessageDialog md = new MessageDialog(message);
                await md.ShowAsync();
            }
            catch { }
        }

        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button b = sender as Button;

                if ((string)b.Content == "Pause")
                {
                    download.Pause();
                    b.Content = "Resume";
                }
                else
                {
                    download.Resume();
                    b.Content = "Pause";
                }
            }
            catch { }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                download.Pause();
                ProgressBar1.Value = 0;
            }
            catch { }
        }

        private void Border_Tapped(object sender, TappedRoutedEventArgs e)
        {
            
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            if (filedownloaded != null && finished)
            {
                LaunchFileAsync();
            }
        }
        
    }
    public static class ExtensionMethods
    {
        public static string ToFileSize(this ulong l)
        {
            return String.Format(new FileSizeFormatProvider(), "{0:fs}", l);
        }
    }
    public class FileSizeFormatProvider : IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter)) return this;
            return null;
        }

        private const string fileSizeFormat = "fs";
        private const Decimal OneKiloByte = 1024M;
        private const Decimal OneMegaByte = OneKiloByte * 1024M;
        private const Decimal OneGigaByte = OneMegaByte * 1024M;

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (format == null || !format.StartsWith(fileSizeFormat))
            {
                return defaultFormat(format, arg, formatProvider);
            }

            if (arg is string)
            {
                return defaultFormat(format, arg, formatProvider);
            }

            Decimal size;

            try
            {
                size = Convert.ToDecimal(arg);
            }
            catch (InvalidCastException)
            {
                return defaultFormat(format, arg, formatProvider);
            }

            string suffix;
            if (size > OneGigaByte)
            {
                size /= OneGigaByte;
                suffix = "GB";
            }
            else if (size > OneMegaByte)
            {
                size /= OneMegaByte;
                suffix = "MB";
            }
            else if (size > OneKiloByte)
            {
                size /= OneKiloByte;
                suffix = "kB";
            }
            else
            {
                suffix = " B";
            }

            string precision = format.Substring(2);
            if (String.IsNullOrEmpty(precision)) precision = "2";
            return String.Format("{0:N" + precision + "}{1}", size, suffix);

        }

        private static string defaultFormat(string format, object arg, IFormatProvider formatProvider)
        {
            IFormattable formattableArg = arg as IFormattable;
            if (formattableArg != null)
            {
                return formattableArg.ToString(format, formatProvider);
            }
            return arg.ToString();
        }

    }
}

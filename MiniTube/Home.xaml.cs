﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YouTube.API;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniTube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Home : Page
    {
        public Home()
        {
            this.InitializeComponent();
            
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            feedsCollection.Source = e.Parameter;
        }
        private async void mainGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            LocalVideoParamenter p = new LocalVideoParamenter();
            if (e.ClickedItem is YouTube.API.DataModel.Search.Item)
            {
                var item = e.ClickedItem as YouTube.API.DataModel.Search.Item;
                if(item.id.kind == "youtube#playlist")
                {
                    var data = await PlaylistItems.List(new YouTube.API.Parameters.PlaylistItemsParemeters()
                    {
                        part = "snippet",
                        playlistId = item.id.playlistId
                    });
                    Frame.Navigate(typeof(ViewPlaylist), data.items);
                    return;
                }
                else
                    p.SearchVideo = item;
            }
            if (e.ClickedItem is YouTube.API.DataModel.Videos.Item)
                p.Video = e.ClickedItem;

            Frame.Navigate(typeof(ViewVideo), p);
        }
    }
    public class TileTemplateSelector : DataTemplateSelector
    {
        //These are public properties that will be used in the Resources section of the XAML.
        public DataTemplate VideoTemplate { get; set; }
        public DataTemplate PlaylistTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var i = item as YouTube.API.DataModel.CommonItem;
            if (i.kind == "youtube#searchResult")
            {
                var sitem = i as YouTube.API.DataModel.Search.Item;
                if (sitem.id.kind == "youtube#playlist")
                    return PlaylistTemplate;
                else return VideoTemplate;
            }
            else return VideoTemplate;
        }
    }
}

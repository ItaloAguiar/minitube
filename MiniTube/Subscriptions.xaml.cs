﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YouTube.API;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniTube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SubscriptionsPage : Page
    {
        public SubscriptionsPage()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            try
            {
                foreach (var item in (YouTube.API.DataModel.Subscriptions.Item[])e.Parameter)
                {
                    Partners.Items.Add(item);
                }
            }
            catch { }
        }

        private async void SubscribeBtn_RequestForSubscribe(object sender, YouTube.UI.SubscribeEventArgs e)
        {
            var SubscribeBtn = sender as YouTube.UI.SubscribeButton;
            if (SubscribeBtn.IsSubscribed)
            {
                try
                {
                    var token = await Login.GetToken();
                    await YouTube.API.Subscriptions.Delete(token, e.SubscriptionID);
                    e.Success = false;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Channel", "Unsubscribe", null, 1);

                    var item = SubscribeBtn.Tag;

                    for (int i = 0; i < Partners.Items.Count; i++)
                    {
                        if (Partners.Items[i] == item)
                            Partners.Items.RemoveAt(i);
                    }
                }
                catch
                {
                    SubscribeBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                }
            }            
        }

        private async void Partners_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                MainPage.GetInstance().IsLoading = true;
                var item = e.ClickedItem as YouTube.API.DataModel.Subscriptions.Item;
                List<StandardFeed> feeds = new List<StandardFeed>();
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Search", null, 1);
                ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                feeds.Add(new StandardFeed()
                {
                    Name = string.Format(LanguageLoader.GetString("SearchResults"), item.snippet.title),
                    /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                    Feed = (await YouTube.API.Search.List(new YouTube.API.Parameters.SearchParameters()
                    {
                        channelId = item.snippet.resourceId.channelId,
                        order = YouTube.API.Parameters.SearchParameters.OrderType.date,
                        maxResults = 50,
                        type = "video"

                    }
                    )).items.ToObservableCollection()
                });

                MainPage.GetInstance().SetContent(typeof(Home), feeds);
            }
            finally
            {
                MainPage.GetInstance().IsLoading = false;
            }

        }
    }
    
}
